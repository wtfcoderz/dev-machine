FROM ubuntu:18.04

# Common tools
RUN  apt-get update \
     && apt-get install -y openssh-server gnupg curl vim apt-transport-https ca-certificates software-properties-common \
     && apt-get clean \
     && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# dev user
RUN  useradd -s /bin/bash dev \
     && echo 'dev:dev' | chpasswd \
     && mkdir -p /home/dev/work \
     && chown -R dev: /home/dev

# Docker
RUN apt-get update \
    && curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - \
    && add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
    && apt-get update \
    && apt-get install -y docker-ce \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# Chrome
RUN apt-get update \
    && curl -fsSL https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && echo "deb [arch=amd64] https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list \
    && apt-get update \
    && apt-get install -y vim google-chrome-stable \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# VS Code
RUN apt-get update \
    && curl -fsSL https://go.microsoft.com/fwlink/?LinkID=760868 > vscode.deb \
    && apt-get install -y ./vscode.deb \
    && rm -f vscode.deb \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Sublime Text
RUN apt-get update \
    && curl -fsSL https://download.sublimetext.com/sublimehq-pub.gpg | apt-key add - \
    && echo "deb https://download.sublimetext.com/ apt/stable/" > /etc/apt/sources.list.d/sublime-text.list \
    && apt-get update \
    && apt-get install -y sublime-text \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

WORKDIR /home/dev

EXPOSE 22
COPY   docker-entrypoint.sh /
RUN    chmod 700 /docker-entrypoint.sh
CMD    ["/docker-entrypoint.sh"]
