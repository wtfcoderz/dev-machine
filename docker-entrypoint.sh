#!/bin/bash

mkdir -p /run/sshd
if [ -n "${USER_PASSWORD}" ]; then
  echo "dev:${USER_PASSWORD}" | chpasswd
fi

/usr/sbin/sshd -D
